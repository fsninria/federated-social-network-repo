package org.inria.fsn;

import org.inria.fsn.interfaces.FsnNetwork;
import org.inria.fsn.interfaces.FsnNetworkFactory;

public class FsnFacebookFactory implements FsnNetworkFactory {

	@Override
	public FsnNetwork createFsnNetwork() {
		return new FsnFacebook();
	}
}
