package org.inria.fsn;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.inria.fsn.data.FsnUser;
import org.inria.fsn.server.FsnServer;
import org.inria.fsn.util.LogManager;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterUtility {
	private Twitter twitterInstance;
	private String screenName;
	public TwitterUtility() throws IOException
	{
		ConfigurationBuilder cb = new ConfigurationBuilder();
		Properties props = new Properties();
		FileInputStream in = new FileInputStream("config/twitter.properties");
		props.load(in);
		in.close();
		cb.setDebugEnabled(true)
		  .setOAuthConsumerKey(props.getProperty("consumer.key"))
		  .setOAuthConsumerSecret(props.getProperty("consumer.secret"))
		  .setOAuthAccessToken(props.getProperty("accesstoken"))
		  .setOAuthAccessTokenSecret(props.getProperty("accesstoken.secret"));
		this.screenName = props.getProperty("account.screenname");
		twitterInstance = new TwitterFactory(cb.build()).getInstance();
	}

	public String getOwnScreenName()
	{
		return this.screenName;
	}

	public boolean isReplyValid(long originalTweetId, String replyOwnerScreenName, String replyText)
	{
		try
		{
			Status st = this.twitterInstance.showStatus(originalTweetId);
			String tweetText = st.getText();
			tweetText = tweetText.replace("allow-follow ", "")
								 .replace("?YES or NO", "")
								 .replace("@", "");
			boolean response = (replyOwnerScreenName.toLowerCase().equals(tweetText.split(" ")[0].toLowerCase()) && (replyText.toLowerCase().contains("yes")||replyText.toLowerCase().contains("no")));
			if(response)
				LogManager.log("Reply from "+replyOwnerScreenName+" is valid");
			else
				LogManager.log("Reply from "+replyOwnerScreenName+" is not valid");
			return response;
		}
		catch(TwitterException e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error validating reply from @"+replyOwnerScreenName);
		}
		return false;
	}

	public FsnUser getFollowerFromTweet(long tweetId)
	{
		FsnUser fsnU = null;
		try
		{
			Status st = this.twitterInstance.showStatus(tweetId);
			String tweetText = st.getText();
			tweetText = tweetText.replace("allow-follow ", "")
								 .replace("?YES or NO", "");
			fsnU = new FsnUser(tweetText.split(" ")[1].split(":")[0], "", "", tweetText.split(" ")[1].split(":")[1]);
			LogManager.log("Got the follower "+fsnU.getUsername()+" from tweetId "+tweetId);
		}
		catch(TwitterException e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error getting follower from tweetId "+tweetId);
		}
		return fsnU;
	}
}
