package org.inria.fsn;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Properties;
import java.util.Queue;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.inria.fsn.data.FeedObject;
import org.inria.fsn.data.FsnUser;
import org.inria.fsn.db.ConnectionManager;
import org.inria.fsn.interfaces.FsnNetwork;
import org.inria.fsn.server.FsnServer;
import org.inria.fsn.util.LogManager;

import com.restfb.DefaultFacebookClient;
import com.restfb.Facebook;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.FacebookType;
import com.restfb.types.Post;
import com.restfb.types.User;

public class FsnFacebook implements FsnNetwork{
	private static FacebookClient facebookClient;

	private String getAppAccessToken()
	{
		String at = null;
		try 
		{
			Properties props = new Properties();
			FileInputStream in = new FileInputStream("config/facebook.properties");
			props.load(in);
			in.close();
			String old_at = props.getProperty("app_access_token");
			String appId = props.getProperty("client_id");
			String appSecret = props.getProperty("client_secret");;
			at = new DefaultFacebookClient().obtainExtendedAccessToken(appId, appSecret, old_at).getAccessToken();
			props.setProperty("app_access_token", at);
			props.store(new FileOutputStream(new File("config/facebook.properties")), ""); 
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error getting app access token for facebook");
		}
		return at;
	}

	public String getUserAccessToken(String username, String network)
	{
		Connection conn = null;
		Statement stat = null;
		ResultSet rs = null;
		String accessToken = null;
		try
		{
			conn = ConnectionManager.getConnection();
			stat = conn.createStatement();
			rs = stat.executeQuery("SELECT accesstoken FROM accesstoken WHERE username='"+username+"'");
			if(rs.next())
			{
				accessToken = rs.getString("accesstoken");
			}
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Cannot get access token of user "+username);
		}
		finally
		{
			try
			{
				if(rs!=null)
					rs.close();
				if(stat!=null)
					stat.close();
				if(conn!=null)
					conn.close();
        	} 
			catch (SQLException se)
			{
				if(FsnServer.debug)
					LogManager.log(se, "Cannot close the database connections");
			}
		}
		return accessToken;
	}

	public void post(String username, String accessToken, String message)
	{
		try
		{
			facebookClient = new DefaultFacebookClient(accessToken);
			facebookClient.publish(username+"/feed", FacebookType.class, Parameter.with("message", message));
			LogManager.log("Posted \""+message+"\" on "+username+"'s facebook profile.");
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Cannot post on "+username+"'s profile");
		}
	}

	public List<FsnUser> search(String user)
	{
		List<FsnUser> results = new ArrayList<FsnUser>();
		String accessToken = this.getAppAccessToken();
		try
		{
			facebookClient = new DefaultFacebookClient(accessToken);
			com.restfb.Connection<User> ps = facebookClient.fetchConnection("search", User.class,
				    Parameter.with("q", user), Parameter.with("type", "user"), Parameter.with("fields", "id,first_name,last_name,username"));
			for(User u : ps.getData())
			{
				FsnUser fsnu = null;
				if(u.getUsername()==null)
					fsnu = new FsnUser(u.getId(), u.getFirstName(), u.getLastName(), "facebook");
				else
					fsnu = new FsnUser(u.getUsername(), u.getFirstName(), u.getLastName(), "facebook");
				results.add(fsnu);
			}
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error searching user \""+user+"\"");
		}
		return results;
	}

	public String follow(String user, String accessToken, FsnUser requester)
	{
		try
		{
			facebookClient = new DefaultFacebookClient(accessToken);
			AppRequestResponse apprequest = facebookClient.publish(user+"/apprequests",
				AppRequestResponse.class,
	            Parameter.with("message", requester.getUsername()+" from "+requester.getNetwork()+" wants to follow you"),
	            Parameter.with("data", requester.getNetwork()+":"+requester.getUsername())
	        );
			LogManager.log("Sent facebook app request to "+apprequest.to+" on behalf of "+requester.getNetwork()+":"+requester.getUsername());
			return apprequest.request;
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error searching user \""+user+"\"");
		}
		return null;
	}

	@Override
	public void startListening(Queue<FeedObject> q) {
		FsnFacebookStreamListener fl = new FsnFacebookStreamListener(q);
		fl.start();
	}

	public List<FeedObject> getFeed(String username, String accessToken, Date since)
	{
		List<FeedObject> al = new ArrayList<FeedObject>();
		try {
			facebookClient = new DefaultFacebookClient(accessToken);
			com.restfb.Connection<Post> filteredFeed =
		        facebookClient.fetchConnection("me/feed", Post.class, Parameter.with("since", since));
			if(filteredFeed!=null)
			for(Post p : filteredFeed.getData())
			{
				if(p!=null && p.getMessage()!=null)
				{
					al.add(new FeedObject(username, "facebook", "", "", p.getMessage()));
				}
			}
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error getting feed for user \""+username+"\"");
		}
		return al;
	}

//	@Override
//	public void notify(String user, String uat, String message) {
//		try
//		{
//			facebookClient = new DefaultFacebookClient(uat);
//			facebookClient.publish("me/notifications",
//                    FacebookType.class,
//                    Parameter.with("access_token", this.getAppAccessToken()),
//                    Parameter.with("template", message),
//                    Parameter.with("href", "https://www.facebook.com/appcenter/requests")
//            );
//	    }
//	    catch(Exception e) {
//	    	if(FsnServer.debug)
//				LogManager.log(e, "Error sending notification to user \""+user+"\"");
//	    }
//	}

	public void notify(String username, String accessToken, String m)
	{
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://facebook.com/"+username+"/notifications");
	
			// Request parameters and other properties.
			List<NameValuePair> params = new ArrayList<NameValuePair>(2);
			params.add(new BasicNameValuePair("access_token", this.getAppAccessToken()));
			params.add(new BasicNameValuePair("template", m));
			params.add(new BasicNameValuePair("href", "https://www.facebook.com/appcenter/requests"));
			httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
	
			//Execute and get the response.
			httpclient.execute(httppost);
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error notifying user "+username);
		}
	}

	@Override
	public void sendMessage(String user, String accessToken, String sender, String senderNetwork, String m) {
		Connection conn = null;
		Statement stat = null;
		try
		{
			conn = ConnectionManager.getConnection();
			stat = conn.createStatement();
			stat.executeUpdate("INSERT INTO messages values('"+sender+"', '"+user+"', '"+senderNetwork+"', 'facebook', '"+m+"')");
			LogManager.log("Saved message for "+user+":facebook from "+sender+":"+senderNetwork);
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error in Twitter Stream");
		}
		finally
		{
			try
			{
				if(stat!=null)
					stat.close();
				if(conn!=null)
					conn.close();
        	} 
			catch (SQLException se) 
			{
				if(FsnServer.debug)
					se.printStackTrace();
			}
		}
	}
	
}
class AppRequestResponse extends FacebookType
{
    private static final long serialVersionUID = 1L;

    public AppRequestResponse()
    {

    }

    @Facebook
    public String request;

    @Facebook
    public String to;
}
