package org.inria.fsn.interfaces;

import java.io.IOException;

public interface FsnNetworkFactory {
	public FsnNetwork createFsnNetwork() throws IOException;
}
