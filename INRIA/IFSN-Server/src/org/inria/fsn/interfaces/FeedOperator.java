package org.inria.fsn.interfaces;

import java.io.IOException;

import org.inria.fsn.FsnFacebookFactory;
import org.inria.fsn.FsnTwitterFactory;

public abstract class FeedOperator implements Runnable{
	protected FsnNetwork createNetwork(String name) throws IOException
	{
		FsnNetwork network = null;
		if(name.equals("facebook"))
			network = new FsnFacebookFactory().createFsnNetwork();
		else if(name.equals("twitter"))
			network = new FsnTwitterFactory().createFsnNetwork();
		return network;
	}
}
