package org.inria.fsn.interfaces;

import java.util.List;
import java.util.Queue;

import org.inria.fsn.data.FeedObject;
import org.inria.fsn.data.FsnUser;

public interface FsnNetwork {
	public String getUserAccessToken(String username, String network);
	public void post(String recepient, String accessToken, String message);
	public String follow(String user, String accessToken, FsnUser requester);
	public void sendMessage(String user, String accessToken, String sender, String senderNetwork, String m);
	public void notify(String username, String userAccessToken, String message);
	public List<FsnUser> search(String keyword);
	public void startListening(Queue<FeedObject> q);
}
