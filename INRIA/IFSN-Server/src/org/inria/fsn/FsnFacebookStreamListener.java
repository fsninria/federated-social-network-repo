package org.inria.fsn;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Queue;

import org.inria.fsn.data.FeedObject;
import org.inria.fsn.db.ConnectionManager;
import org.inria.fsn.server.FsnServer;
import org.inria.fsn.util.LogManager;

import com.restfb.DefaultFacebookClient;

public class FsnFacebookStreamListener extends Thread {
	private Queue<FeedObject> queue;
	public FsnFacebookStreamListener(Queue<FeedObject> q)
	{
		this.queue = q;
	}

	private void keepAccessTokenAlive()
	{
		try 
		{
			Properties props = new Properties();
			FileInputStream in = new FileInputStream("config/facebook.properties");
			props.load(in);
			in.close();
			String old_at = props.getProperty("app_access_token");
			String appId = props.getProperty("client_id");
			String appSecret = props.getProperty("client_secret");;
			String at = new DefaultFacebookClient().obtainExtendedAccessToken(appId, appSecret, old_at).getAccessToken();
			props.setProperty("app_access_token", at);
			props.store(new FileOutputStream(new File("config/facebook.properties")), ""); 
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error extending the app access token for facebook");
		}
	}

	public void run()
	{
		Connection conn = null;
		Statement stat  = null;
		ResultSet rs    = null;
		try
		{
			conn = ConnectionManager.getConnection();
			Date lastCheck = new Date();
			while(true)
			{
				this.keepAccessTokenAlive();
				try
				{
					stat = conn.createStatement();
					rs = stat.executeQuery("SELECT * FROM accesstoken");
					while(rs.next())
					{
						String fUser = rs.getString("username");
						String fAccessToken = rs.getString("accesstoken");
	
						// Take out the feed from facebook
						FsnFacebook ff = new FsnFacebook();
						List<FeedObject> al = ff.getFeed(fUser, fAccessToken, lastCheck);
	
						Statement stat1 = conn.createStatement();
						ResultSet rs1 = stat1.executeQuery("SELECT * from accesscontrol WHERE username='"+fUser+"' AND status='accepted'");
						while(rs1.next())
						{
							String follower = rs1.getString("follower");
							String followerNetwork = rs1.getString("followernetwork");
							for(FeedObject f : al)
							{
								FeedObject rfo = new FeedObject(f.getFromUser(), f.getFromNetwork(), follower, followerNetwork, f.getMessage());
								queue.offer(rfo);
							}
						}
						synchronized (queue) {
		                    queue.notifyAll();
		                }
						rs1.close();
						stat1.close();
					}
					lastCheck = new Date();
					sleep(100000);
				}
				catch(Exception e)
				{
					if(FsnServer.debug)
						LogManager.log(e, "Error in FsnFacebookStreamListener");
				}
			}
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error getting database connection");
		}
		finally
		{
			try {
				if(!conn.isClosed())
					conn.close();
				if(!stat.isClosed())
					stat.close();
			}
			catch(Exception e)
			{
				LogManager.log(e, "Error terminating database connections");
			}
		}
	}
}
