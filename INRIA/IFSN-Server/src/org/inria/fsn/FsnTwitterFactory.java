package org.inria.fsn;

import java.io.IOException;

import org.inria.fsn.interfaces.FsnNetwork;
import org.inria.fsn.interfaces.FsnNetworkFactory;

public class FsnTwitterFactory implements FsnNetworkFactory{

	@Override
	public FsnNetwork createFsnNetwork() throws IOException{
		return new FsnTwitter();
	}
}
