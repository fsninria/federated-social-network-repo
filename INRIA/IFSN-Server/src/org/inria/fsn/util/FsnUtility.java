package org.inria.fsn.util;

import java.io.IOException;

import org.inria.fsn.FsnFacebookFactory;
import org.inria.fsn.FsnTwitterFactory;
import org.inria.fsn.interfaces.FsnNetwork;

public class FsnUtility {
	public static FsnNetwork createNetwork(String name) throws IOException
	{
		FsnNetwork network = null;
		if(name.equals("facebook"))
			network = new FsnFacebookFactory().createFsnNetwork();
		else if(name.equals("twitter"))
			network = new FsnTwitterFactory().createFsnNetwork();
		return network;
	}
}
