package org.inria.fsn.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class LogManager {
	private static String path = "logs/";
	protected static String lastlog = "";

    public static void log(Exception e, String message){
        Date dt = new Date();
        java.sql.Date now = new java.sql.Date(dt.getTime());
        lastlog = "[" + dt.toString() + "] "+message+": "+e.toString()+"\r\n";
        for(StackTraceElement ste : e.getStackTrace())
        {
        	lastlog+="\t"+ste.toString()+"\r\n";
        }
        try {
            FileOutputStream fw = new FileOutputStream(path+now.toString()+".txt", true);
            fw.write(lastlog.getBytes());
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void log(String message)
    {
    	Date dt = new Date();
        java.sql.Date now = new java.sql.Date(dt.getTime());
        lastlog = "[" + dt.toString() + "] " + message+"\r\n";
        try {
            FileOutputStream fw = new FileOutputStream(path+now.toString()+".txt", true);
            fw.write(lastlog.getBytes());
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
