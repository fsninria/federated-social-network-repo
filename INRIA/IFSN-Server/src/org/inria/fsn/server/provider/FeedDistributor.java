package org.inria.fsn.server.provider;

import java.io.IOException;
import java.util.Queue;

import org.inria.fsn.data.FeedObject;
import org.inria.fsn.interfaces.FeedOperator;
import org.inria.fsn.interfaces.FsnNetwork;
import org.inria.fsn.server.FsnServer;
import org.inria.fsn.util.LogManager;

public class FeedDistributor extends FeedOperator{
	private final Queue<FeedObject> queue;

	public FeedDistributor(Queue<FeedObject> q)
	{
		this.queue = q;
	}

	@Override
	public void run() {
		while (true) {
            consume();
            try {
                synchronized (queue) {
                    queue.wait();
                }
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
	}

	private void consume()
	{
		while (!queue.isEmpty()) {
			FeedObject fo = queue.poll();
            if (fo != null) {
            	try {
            		FsnNetwork network = this.createNetwork(fo.getToNetwork());
            		String message = fo.getFromUser()+" posted on "+fo.getFromNetwork()+": "+fo.getMessage();
            		network.post(fo.getToUser(), network.getUserAccessToken(fo.getToUser(), fo.getToNetwork()), message);
            	}
            	catch(IOException e)
            	{
            		if(FsnServer.debug)
            			LogManager.log(e, "Error creating network object");
            	}
            }
		}
	}
}
