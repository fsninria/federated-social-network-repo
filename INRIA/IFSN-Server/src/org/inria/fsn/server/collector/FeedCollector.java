package org.inria.fsn.server.collector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import org.inria.fsn.FsnFacebook;
import org.inria.fsn.FsnTwitter;
import org.inria.fsn.data.FeedObject;
import org.inria.fsn.interfaces.FeedOperator;
import org.inria.fsn.interfaces.FsnNetwork;
import org.inria.fsn.server.FsnServer;
import org.inria.fsn.util.LogManager;

public class FeedCollector extends FeedOperator {
	private final Queue<FeedObject> queue;


	public FeedCollector(Queue<FeedObject> q)
	{
		this.queue = q;
	}

	@Override
	public void run() {
		List<FsnNetwork> networks = this.getNetworkList();
		for(FsnNetwork n : networks)
		{
			n.startListening(queue);
		}
	}

	private List<FsnNetwork> getNetworkList()
	{
		List<FsnNetwork> networks = new ArrayList<FsnNetwork>();
		try {
			networks.add(new FsnTwitter());
			networks.add(new FsnFacebook());
		} catch (IOException e) {
			if(FsnServer.debug)
				LogManager.log(e, "Cannot get the list of networks");
		}
		return networks;
	}
}
