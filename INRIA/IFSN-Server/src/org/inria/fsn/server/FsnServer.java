package org.inria.fsn.server;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.inria.fsn.data.FeedObject;
import org.inria.fsn.interfaces.FeedOperator;
import org.inria.fsn.server.collector.FeedCollector;
import org.inria.fsn.server.provider.FeedDistributor;
import org.inria.fsn.util.LogManager;

public class FsnServer {

	public static boolean debug;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String log = "Starting application";
		debug = false;
		Properties props = new Properties();
        FileInputStream in;
		try {
			in = new FileInputStream("config/application.properties");
			props.load(in);
	        in.close();
	        if(props.getProperty("debug").equals("true"))
	        {
	        	debug = true;
	        	log+=" in DEBUG mode";
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}

		Queue<FeedObject> queue = new ConcurrentLinkedQueue<FeedObject>();
		FeedOperator collector = new FeedCollector(queue);
        FeedOperator distributor = new FeedDistributor(queue);

        Thread t1 = new Thread(collector);
        Thread t2 = new Thread(distributor);

        LogManager.log(log);
        t1.start();
        t2.start();
	}

}
