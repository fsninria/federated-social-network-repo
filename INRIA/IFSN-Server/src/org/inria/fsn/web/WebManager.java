package org.inria.fsn.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class WebManager {

    public static String getDocument(String url) throws IOException
    {
    	StringBuilder content = new StringBuilder();
		URL urlObject = new URL(url.replaceAll(" ", "%20"));
		URLConnection urlConnection = urlObject.openConnection();
		urlConnection.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

		String line;
		while ((line = bufferedReader.readLine()) != null)
		{ 
			content.append(line + "\n");
		}
		bufferedReader.close();
        return content.toString();
    }    
}
