package org.inria.fsn;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Queue;

import org.inria.fsn.data.FeedObject;
import org.inria.fsn.data.FsnUser;
import org.inria.fsn.db.ConnectionManager;
import org.inria.fsn.interfaces.FsnNetwork;
import org.inria.fsn.server.FsnServer;
import org.inria.fsn.util.FsnUtility;
import org.inria.fsn.util.LogManager;

import twitter4j.DirectMessage;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.User;
import twitter4j.UserList;
import twitter4j.UserStreamListener;

public class FsnTwitterStreamListener implements UserStreamListener{
	private final Queue<FeedObject> queue;
	public FsnTwitterStreamListener(Queue<FeedObject> q)
	{
		this.queue = q;
	}

	private void followIfNotAlready(String screenName)
	{
		try
		{
			FsnTwitter twitter = new FsnTwitter();
			twitter.follow(screenName, "", null);
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error initializing FsnTwitter");
		}
	}

	@Override
    public void onStatus(Status status) {
		Connection conn = null;
		Statement stat = null;
		ResultSet rs = null;
		try
		{
			TwitterUtility util = new TwitterUtility();
			if(!status.getUser().getScreenName().toLowerCase().equals(util.getOwnScreenName().toLowerCase()))
			{
				this.followIfNotAlready(status.getUser().getScreenName());
				String sp[] = status.getText().toLowerCase().split(" ");
				if(sp.length>0 && sp[0].equals("@"+util.getOwnScreenName().toLowerCase()))
				{
					if(sp.length>1)
					{
						String cmd = sp[1];
						FsnTwitter twitter = new FsnTwitter();
						if(cmd.contains("yes"))
						{
							long originalTweetId = status.getInReplyToStatusId();
							if(originalTweetId>=0 && util.isReplyValid(originalTweetId, status.getUser().getScreenName().toLowerCase(), status.getText()))
			            	{
								conn = ConnectionManager.getConnection();
			            		FsnUser fsnu = util.getFollowerFromTweet(originalTweetId);
			            		stat  = conn.createStatement();
			            		stat.executeUpdate("UPDATE accesscontrol SET status='accepted' WHERE username='"+status.getUser().getScreenName().toLowerCase()+"' AND follower='"+fsnu.getUsername()+"' AND usernetwork='twitter' AND followernetwork='"+fsnu.getNetwork()+"'");
			            	}
						}
						else if(sp.length>3)
						{
							String network = sp[2];
							String keyword = sp[3];
							if(cmd.equals("search"))
							{
								FsnNetwork nw = FsnUtility.createNetwork(network);
								StringBuffer message = new StringBuffer("Results: ");

								if(nw!=null)
								{
									List<FsnUser> al = nw.search(keyword);
									for(FsnUser fu : al)
									{
										message.append(fu.getUsername()+" ");
									}
								}
								String m = null;
								if(message.length()-status.getUser().getScreenName().length()-1>140)
								{
									m = message.substring(0,140);
									m = m.substring(0, m.lastIndexOf(" ")-status.getUser().getScreenName().length()-1);
								}
								else
									m = message.toString();
								if(!m.equals("Results: "))
									twitter.post(status.getUser().getScreenName(), "", m);
								else
									twitter.post(status.getUser().getScreenName(), "", "No results found for \""+keyword+"\"");
							}
							else if(cmd.equals("add"))
							{
								FsnNetwork nw = FsnUtility.createNetwork(network);
								FsnUser fu = new FsnUser(status.getUser().getScreenName().toLowerCase(), "", "", "twitter");
								if(conn==null)
									conn = ConnectionManager.getConnection();
								stat = conn.createStatement();
								rs = stat.executeQuery("SELECT accesstoken FROM accesstoken WHERE username='"+keyword+"'");
								if(rs.next())
								{
									String at = rs.getString("accesstoken");
									String reference = nw.follow(keyword, at, fu);
									if(conn==null)
					            		conn = ConnectionManager.getConnection();
							        stat  = conn.createStatement();
							        stat.executeUpdate("INSERT INTO accesscontrol values('"+keyword+"', '"+fu.getUsername()+"', '"+network+"', '"+fu.getNetwork()+"', '"+reference+"', 'pending')");
							        nw.notify(keyword, "AAAH0ZBNNZAFrkBAH54yU14dHC7W4bBApCKXg6l8fZCf9PgY0QimjExhXfGhsSfGkrmKQUlG1PYBDliTpz7lhjsuSS56ZCwyy3AWSxAypNgrYxc27VlPT", fu.getUsername()+" from "+fu.getNetwork()+" wants to follow you");
							        twitter.post(fu.getUsername(), "", "Request sent to user "+keyword+" successfully");
								}
								else
								{
									LogManager.log("Error sending request to "+keyword+". AccessToken for user was not found in the database.");
									twitter.post(fu.getUsername(), "", "Error sending request to "+keyword+". Please make sure the user exists and is registered with IFSN app on "+network);
								}
							}
						}
					}
				}
				else
	            {
	            	String screenName = status.getUser().getScreenName().toLowerCase();
	            	if(conn==null)
	            		conn = ConnectionManager.getConnection();
			        stat  = conn.createStatement();
			        rs = stat.executeQuery("SELECT * FROM accesscontrol WHERE username='"+screenName+"' AND status='accepted'");
			        try {
						while(rs.next()) {
							String follower = rs.getString("follower");
							FeedObject fo = new FeedObject(screenName, "twitter", follower, rs.getString("followernetwork"), status.getText());
							queue.offer(fo);
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
	        		synchronized (queue) {
	                    queue.notifyAll();
	                }
	            }
			}
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error in Twitter Stream");
		}
		finally
		{
			try
			{
				if(rs!=null)
					rs.close();
				if(stat!=null)
					stat.close();
				if(conn!=null)
					conn.close();
        	} 
			catch (SQLException se) 
			{
				if(FsnServer.debug)
					se.printStackTrace();
			}
		}
    }

	@Override
	public void onBlock(User arg0, User arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDeletionNotice(long arg0, long arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDirectMessage(DirectMessage message) {
		Connection conn = null;
		Statement stat = null;
		ResultSet rs = null;
		try
		{
			TwitterUtility util = new TwitterUtility();
			String sender = message.getSender().getScreenName().toLowerCase();
			if(!sender.equals(util.getOwnScreenName().toLowerCase()))
			{
				this.followIfNotAlready(sender);
				String sp[] = message.getText().toLowerCase().split(" ");
				if(sp.length>=3)
				{
					FsnTwitter twitter = new FsnTwitter();
					String network  = sp[0];
					String follower = sp[1];
					String m = "";
					for(int i=2;i<sp.length;i++)
					{
						m+=sp[i];
						if(i!=sp.length-1)
							m+=" ";
					}
					conn = ConnectionManager.getConnection();
            		stat  = conn.createStatement();
            		rs = stat.executeQuery("SELECT status FROM accesscontrol WHERE username='"+sender+"' AND follower='"+follower+"' AND usernetwork='twitter' AND followernetwork='"+network+"'");
            		if(rs.next() && rs.getString("status").equals("accepted"))
            		{
            			FsnNetwork nw = FsnUtility.createNetwork(network);
            			nw.sendMessage(follower, "", sender, "twitter", m);
            		}
            		else
            		{
            			twitter.sendMessage(sender, "", "", "", follower+" from "+network+" is not following you. To receive a message, the user must first follow you");
            		}
				}
			}
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error in Twitter Stream");
		}
		finally
		{
			try
			{
				if(rs!=null)
					rs.close();
				if(stat!=null)
					stat.close();
				if(conn!=null)
					conn.close();
        	} 
			catch (SQLException se) 
			{
				if(FsnServer.debug)
					se.printStackTrace();
			}
		}
	}

	@Override
	public void onFavorite(User arg0, User arg1, Status arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFollow(User arg0, User arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFriendList(long[] arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUnblock(User arg0, User arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUnfavorite(User arg0, User arg1, Status arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUserListCreation(User arg0, UserList arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUserListDeletion(User arg0, UserList arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUserListMemberAddition(User arg0, User arg1,
			UserList arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUserListMemberDeletion(User arg0, User arg1,
			UserList arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUserListSubscription(User arg0, User arg1,
			UserList arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUserListUnsubscription(User arg0, User arg1,
			UserList arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUserListUpdate(User arg0, UserList arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUserProfileUpdate(User arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDeletionNotice(StatusDeletionNotice arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onScrubGeo(long arg0, long arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStallWarning(StallWarning arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTrackLimitationNotice(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onException(Exception arg0) {
		// TODO Auto-generated method stub
		
	}
}
