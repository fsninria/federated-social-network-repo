package org.inria.fsn;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Queue;

import org.inria.fsn.data.FeedObject;
import org.inria.fsn.data.FsnUser;
import org.inria.fsn.interfaces.FsnNetwork;
import org.inria.fsn.server.FsnServer;
import org.inria.fsn.util.LogManager;

import twitter4j.DirectMessage;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

public class FsnTwitter implements FsnNetwork{
	private Twitter twitterInstance;

	public String getUserAccessToken(String u, String n)
	{
		return null;
	}

	public FsnTwitter() throws IOException
	{
		ConfigurationBuilder cb = new ConfigurationBuilder();
		Properties props = new Properties();
		FileInputStream in = new FileInputStream("config/twitter.properties");
		props.load(in);
		in.close();
		cb.setDebugEnabled(true)
		  .setOAuthConsumerKey(props.getProperty("consumer.key"))
		  .setOAuthConsumerSecret(props.getProperty("consumer.secret"))
		  .setOAuthAccessToken(props.getProperty("accesstoken"))
		  .setOAuthAccessTokenSecret(props.getProperty("accesstoken.secret"));
		twitterInstance = new TwitterFactory(cb.build()).getInstance();
	}

	public void post(String u, String at, String m)
	{
		try
		{
			if(u!=null)
				m="@"+u+" "+m;
			if(m.length()>140)
				m = m.substring(0, 139);
			Status status = this.twitterInstance.updateStatus(m);
			LogManager.log("Tweeted \""+status.getText()+"\"");
		}
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error tweeting "+m);
		}
	}

	public String follow(String screenName, String accessToken, FsnUser requester)
	{
		try {
            this.twitterInstance.createFriendship(screenName);
            LogManager.log("Following @" + screenName);
        }
		catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error following @"+screenName);
		}
		return null;
	}

	public List<FsnUser> search(String user)
	{
		List<FsnUser> al = new ArrayList<FsnUser>();
		try
		{
			int page = 1;
			ResponseList<User> users;
			do
			{
                users = this.twitterInstance.searchUsers(user, page);
                for (User u : users)
                    al.add(new FsnUser(u.getScreenName(), u.getName(), "", "twitter"));
                page++;
            }
			while (users.size() != 0 && page < 10);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return al;
	}

	public void startListening(Queue<FeedObject> q)
	{
		FileInputStream in;
	    Properties props = new Properties();
		try {
			in = new FileInputStream("config/twitter.properties");
			props.load(in);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		  .setOAuthConsumerKey(props.getProperty("consumer.key"))
		  .setOAuthConsumerSecret(props.getProperty("consumer.secret"))
		  .setOAuthAccessToken(props.getProperty("accesstoken"))
		  .setOAuthAccessTokenSecret(props.getProperty("accesstoken.secret"));

		TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
        twitterStream.addListener(new FsnTwitterStreamListener(q));
        twitterStream.user();
	}

	@Override
	public void notify(String username, String uat, String message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendMessage(String user, String accessToken, String sender, String senderNetwork, String m) {
		try {
			if(sender!=null && !sender.equals("null") && !sender.equals(""))
				m = sender+" from "+senderNetwork+" says: "+m;
            DirectMessage message = this.twitterInstance.sendDirectMessage(user, m);
            LogManager.log("Sent DirectMessage to " + message.getRecipientScreenName());
        }
        catch(Exception e)
		{
			if(FsnServer.debug)
				LogManager.log(e, "Error sending DirectMessage to @"+user);
		}
	}
}
