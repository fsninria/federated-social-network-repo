cd build/classes
rm -rf ../../dist/*
javac -d . -classpath "../../lib/*" ../../src/org/inria/fsn/*.java ../../src/org/inria/fsn/data/*.java ../../src/org/inria/fsn/db/*.java ../../src/org/inria/fsn/interfaces/*.java ../../src/org/inria/fsn/regex/*.java ../../src/org/inria/fsn/server/*.java ../../src/org/inria/fsn/server/collector/*.java ../../src/org/inria/fsn/server/provider/*.java ../../src/org/inria/fsn/util/*.java ../../src/org/inria/fsn/web/*.java
jar cvfm ../../dist/IFSN-Server.jar ../MANIFEST ./*