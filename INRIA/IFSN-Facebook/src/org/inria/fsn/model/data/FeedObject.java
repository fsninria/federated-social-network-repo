package org.inria.fsn.model.data;

public class FeedObject {
	private String fromUser;
	private String fromNetwork;
	private String toUser;
	private String toNetwork;
	private String message;

	public FeedObject(String from, String fromNetwork, String to, String toNetwork, String m)
	{
		this.fromUser    = from;
		this.fromNetwork = fromNetwork;
		this.toUser      = to;
		this.toNetwork   = toNetwork;
		this.message     = m;
	}

	public String getFromUser() {
		return fromUser;
	}

	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}

	public String getFromNetwork() {
		return fromNetwork;
	}

	public void setFromNetwork(String fromNetwork) {
		this.fromNetwork = fromNetwork;
	}

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public String getToNetwork() {
		return toNetwork;
	}

	public void setToNetwork(String toNetwork) {
		this.toNetwork = toNetwork;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
