package org.inria.fsn.model.data;

public class FsnMessage {
	private String toUser;
	private String fromUser;
	private String toNetwork;
	private String fromNetwork;
	private String message;

	public FsnMessage(String toUser, String fromUser, String toNetwork, String fromNetwork, String message)
	{
		this.toUser      = toUser;
		this.fromUser    = fromUser;
		this.toNetwork   = toNetwork;
		this.fromNetwork = fromNetwork;
		this.message     = message;
	}

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public String getFromUser() {
		return fromUser;
	}

	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}

	public String getToNetwork() {
		return toNetwork;
	}

	public void setToNetwork(String toNetwork) {
		this.toNetwork = toNetwork;
	}

	public String getFromNetwork() {
		return fromNetwork;
	}

	public void setFromNetwork(String fromNetwork) {
		this.fromNetwork = fromNetwork;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
