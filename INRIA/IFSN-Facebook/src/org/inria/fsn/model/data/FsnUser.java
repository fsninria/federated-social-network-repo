package org.inria.fsn.model.data;

public class FsnUser {
	private String username;
	private String firstName;
	private String lastName;
	private String network;
	private String status;

	public FsnUser(String u, String f, String l, String n)
	{
		this.username  = u;
		this.firstName = f;
		this.lastName  = l;
		this.network   = n;
	}

	public String getStatus()
	{
		return this.status;
	}

	public void setStatus(String s)
	{
		this.status = s;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
}
