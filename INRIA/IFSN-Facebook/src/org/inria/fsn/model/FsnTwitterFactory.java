package org.inria.fsn.model;

import java.io.IOException;

import org.inria.fsn.model.interfaces.FsnNetwork;
import org.inria.fsn.model.interfaces.FsnNetworkFactory;

public class FsnTwitterFactory implements FsnNetworkFactory{

	@Override
	public FsnNetwork createFsnNetwork() throws IOException{
		return new FsnTwitter();
	}
}
