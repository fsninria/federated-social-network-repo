package org.inria.fsn.model.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionManager 
{
    public static Properties props = null;

    private ConnectionManager() throws ClassNotFoundException
    {
    }

    public static Connection getConnection() throws IOException, SQLException, ClassNotFoundException
	{
        if(props==null)
		{
            props = new Properties();
			props = new Properties();
			Class.forName("com.mysql.jdbc.Driver");
			props.load(new ConnectionManager().getClass().getClassLoader().getResourceAsStream("database.properties"));
        }
        String drivers = props.getProperty("jdbc.drivers");
        if (drivers != null) System.setProperty("jdbc.drivers", drivers);
        String url = props.getProperty("jdbc.url");
        String username = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");
        return DriverManager.getConnection(url, username, password);
    }
}
