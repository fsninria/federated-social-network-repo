package org.inria.fsn.model.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.inria.fsn.model.Configuration;
import org.inria.fsn.model.FsnFacebookFactory;
import org.inria.fsn.model.FsnTwitterFactory;
import org.inria.fsn.model.data.FsnMessage;
import org.inria.fsn.model.data.FsnUser;
import org.inria.fsn.model.db.ConnectionManager;
import org.inria.fsn.model.interfaces.FsnNetwork;

public class FsnUtility {
	public static FsnNetwork createNetwork(String name) throws IOException
	{
		FsnNetwork network = null;
		if(name.equals("facebook"))
			network = new FsnFacebookFactory().createFsnNetwork();
		else if(name.equals("twitter"))
			network = new FsnTwitterFactory().createFsnNetwork();
		return network;
	}

	public static List<FsnUser> getFollowersOf(String username, String network)
	{
		ArrayList<FsnUser> list = new ArrayList<FsnUser>();
		Connection conn = null;
		Statement stat  = null;
		ResultSet rs    = null;
		try
		{
			conn = ConnectionManager.getConnection();
			stat = conn.createStatement();
			rs = stat.executeQuery("SELECT * FROM accesscontrol WHERE username='"+username+"' AND usernetwork='"+network+"'");
			while(rs.next())
			{
				FsnUser fu = new FsnUser(rs.getString("follower"), "", "", rs.getString("followernetwork"));
				list.add(fu);
			}
		}
		catch(Exception e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error getting followers of "+username+":"+network);
		}
		finally
		{
			try
			{
				if(!rs.isClosed())
					rs.close();
				if(!stat.isClosed())
					stat.close();
				if(!conn.isClosed())
					conn.close();
			}
			catch(Exception e)
			{
				if(Configuration.debug)
					LogManager.log(e, "Error terminating database connections");
			}
		}
		return list;
	}

	public static List<FsnUser> getFollowingsOf(String username, String network)
	{
		ArrayList<FsnUser> list = new ArrayList<FsnUser>();
		Connection conn = null;
		Statement stat  = null;
		ResultSet rs    = null;
		try
		{
			conn = ConnectionManager.getConnection();
			stat = conn.createStatement();
			rs = stat.executeQuery("SELECT * FROM accesscontrol WHERE follower='"+username+"' AND followernetwork='"+network+"'");
			while(rs.next())
			{
				FsnUser fu = new FsnUser(rs.getString("username"), "", "", rs.getString("usernetwork"));
				fu.setStatus(rs.getString("status"));
				list.add(fu);
			}
		}
		catch(Exception e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error getting pending requests of "+username+":"+network);
		}
		finally
		{
			try
			{
				if(!rs.isClosed())
					rs.close();
				if(!stat.isClosed())
					stat.close();
				if(!conn.isClosed())
					conn.close();
			}
			catch(Exception e)
			{
				if(Configuration.debug)
					LogManager.log(e, "Error terminating database connections");
			}
		}
		return list;
	}

	public static List<FsnMessage> getMessagesOf(String username, String network)
	{
		ArrayList<FsnMessage> list = new ArrayList<FsnMessage>();
		Connection conn = null;
		Statement stat  = null;
		ResultSet rs    = null;
		try
		{
			conn = ConnectionManager.getConnection();
			stat = conn.createStatement();
			rs = stat.executeQuery("SELECT * FROM messages WHERE receiver='"+username+"' AND receivernetwork='"+network+"'");
			while(rs.next())
			{
				FsnMessage fm = new FsnMessage(rs.getString("receiver"), rs.getString("sender"), rs.getString("receivernetwork"), rs.getString("sendernetwork"), rs.getString("message"));
				list.add(fm);
			}
		}
		catch(Exception e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error getting messages of "+username+":"+network);
		}
		finally
		{
			try
			{
				if(!rs.isClosed())
					rs.close();
				if(!stat.isClosed())
					stat.close();
				if(!conn.isClosed())
					conn.close();
			}
			catch(Exception e)
			{
				if(Configuration.debug)
					LogManager.log(e, "Error terminating database connections");
			}
		}
		return list;
	}
}
