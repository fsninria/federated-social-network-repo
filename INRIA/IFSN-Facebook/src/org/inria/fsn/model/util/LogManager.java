package org.inria.fsn.model.util;

import java.util.Date;

public class LogManager {
	protected static String lastlog = "";

    public static void log(Exception e, String message){
        Date dt = new Date();
        lastlog = "[" + dt.toString() + "] "+message+": "+e.toString()+"\r\n";
        System.out.println(lastlog);
    }

    public static void log(String message)
    {
    	Date dt = new Date();
        lastlog = "[" + dt.toString() + "] "+message+"\r\n";
        System.out.println(lastlog);
    }
}
