package org.inria.fsn.model.interfaces;

import java.io.IOException;

public interface FsnNetworkFactory {
	public FsnNetwork createFsnNetwork() throws IOException;
}
