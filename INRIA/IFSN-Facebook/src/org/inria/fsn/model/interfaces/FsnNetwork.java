package org.inria.fsn.model.interfaces;

import java.util.ArrayList;
import org.inria.fsn.model.data.FsnUser;

public interface FsnNetwork {
	public String getAccessToken(String username, String network);
	public void post(String recepient, String accessToken, String message);
	public void follow(String user);
	public void sendMessage(String user, String accessToken, String sender, String senderNetwork, String m);
	public ArrayList<FsnUser> search(String keyword, String accessToken);
	public String fetchUsername(String accessToken);
}
