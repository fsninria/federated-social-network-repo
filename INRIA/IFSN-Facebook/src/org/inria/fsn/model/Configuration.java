package org.inria.fsn.model;

import java.util.Properties;
import org.inria.fsn.model.util.LogManager;

 public class Configuration {
	public static boolean debug;
	private Configuration()
	{
		
	}
	static {
		Properties props = new Properties();
		try {
			props.load(new Configuration().getClass().getClassLoader().getResourceAsStream("application.properties"));
			String message = "Configuring application";
			if(props.getProperty("debug").equals("true"))
			{
				debug = true;
				message+="in DEBUG mode";
			}
			else
				debug = false;
			LogManager.log(message);
		}
		catch(Exception e)
		{
			LogManager.log(e, "Error loading application configuration");
		}
	}
}