package org.inria.fsn.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.inria.fsn.model.interfaces.FsnNetwork;
import org.inria.fsn.model.data.FsnUser;
import org.inria.fsn.model.db.ConnectionManager;
import org.inria.fsn.model.util.LogManager;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.exception.FacebookException;
import com.restfb.types.FacebookType;
import com.restfb.types.User;

public class FsnFacebook implements FsnNetwork{
	private static FacebookClient facebookClient;

	public String getAccessToken(String username, String network)
	{
		Connection conn = null;
		Statement stat = null;
		ResultSet rs = null;
		String accessToken = null;
		try
		{
			conn = ConnectionManager.getConnection();
			stat = conn.createStatement();
			rs = stat.executeQuery("SELECT accesstoken FROM accesstoken WHERE username='"+username+"'");
			if(rs.next())
			{
				accessToken = rs.getString("accesstoken");
			}
		}
		catch(Exception e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Cannot get access token of user "+username);
		}
		finally
		{
			try
			{
				if(rs!=null)
					rs.close();
				if(stat!=null)
					stat.close();
				if(conn!=null)
					conn.close();
        	} 
			catch (SQLException se)
			{
				if(Configuration.debug)
					LogManager.log(se, "Cannot terminate the database connections");
			}
		}
		return accessToken;
	}

	public void post(String username, String accessToken, String message)
	{
		try
		{
			facebookClient = new DefaultFacebookClient(accessToken);
			facebookClient.publish(username+"/feed", FacebookType.class, Parameter.with("message", message));
			LogManager.log("Posted \""+message+"\" on "+username+"'s facebook profile.");
		}
		catch(FacebookException e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Cannot post on "+username+"'s profile");
		}
	}

	public ArrayList<FsnUser> search(String user, String accessToken)
	{
		ArrayList<FsnUser> results = new ArrayList<FsnUser>();
		try
		{
			facebookClient = new DefaultFacebookClient(accessToken);
			com.restfb.Connection<User> ps = facebookClient.fetchConnection("search", User.class,
				    Parameter.with("q", user), Parameter.with("type", "user"), Parameter.with("fields", "id,first_name,last_name,username"));
			for(User u : ps.getData())
			{
				FsnUser fsnu = null;
				if(u.getUsername()==null)
					fsnu = new FsnUser(u.getId(), u.getFirstName(), u.getLastName(), "facebook");
				else
					fsnu = new FsnUser(u.getUsername(), u.getFirstName(), u.getLastName(), "facebook");
				results.add(fsnu);
			}
		}
		catch(FacebookException e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error searching user \""+user+"\"");
		}
		return results;
	}

	public void follow(String user)
	{
		
	}

	public boolean deleteNotification(String username, String accessToken, String notificationId)
	{
		try
		{
			facebookClient = new DefaultFacebookClient(accessToken);
			return facebookClient.deleteObject("/"+notificationId);
		}
		catch(FacebookException e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error deleting notification for user \""+username+"\"");
		}
		return false;
	}

	public void sendMessage(String user, String accessToken, String sender, String senderNetwork, String m) {
		Connection conn = null;
		Statement stat = null;
		try
		{
			conn = ConnectionManager.getConnection();
			stat = conn.createStatement();
			stat.executeUpdate("INSERT INTO messages values('"+sender+"', '"+user+"', '"+senderNetwork+"', 'facebook', '"+m+"')");
			LogManager.log("Saved message for "+user+":facebook from "+sender+":"+senderNetwork);
		}
		catch(Exception e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error in Twitter Stream");
		}
		finally
		{
			try
			{
				if(stat!=null)
					stat.close();
				if(conn!=null)
					conn.close();
        	} 
			catch (SQLException se) 
			{
				if(Configuration.debug)
					se.printStackTrace();
			}
		}
	}

	public String fetchUsername(String accessToken)
	{
		FacebookClient facebookClient = new DefaultFacebookClient(accessToken);
		User user = facebookClient.fetchObject("me", User.class);
		return user.getUsername();
	}
}
