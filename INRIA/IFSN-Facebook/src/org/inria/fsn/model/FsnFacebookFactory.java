package org.inria.fsn.model;

import org.inria.fsn.model.interfaces.FsnNetwork;
import org.inria.fsn.model.interfaces.FsnNetworkFactory;

public class FsnFacebookFactory implements FsnNetworkFactory {

	@Override
	public FsnNetwork createFsnNetwork() {
		return new FsnFacebook();
	}
}
