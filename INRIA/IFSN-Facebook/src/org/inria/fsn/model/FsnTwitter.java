package org.inria.fsn.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.inria.fsn.model.data.FsnUser;
import org.inria.fsn.model.interfaces.FsnNetwork;
import org.inria.fsn.model.util.LogManager;

import twitter4j.DirectMessage;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

public class FsnTwitter implements FsnNetwork{
	private Twitter twitterInstance;

	public void sendMessage(String user, String accessToken, String sender, String senderNetwork, String m) {
		try {
			if(sender!=null && !sender.equals("null") && !sender.equals(""))
				m = sender+" from "+senderNetwork+" says: "+m;
            DirectMessage message = this.twitterInstance.sendDirectMessage(user, m);
            LogManager.log("Sent DirectMessage to " + message.getRecipientScreenName());
        }
        catch(Exception e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error sending DirectMessage to @"+user);
		}
	}

	public String getAccessToken(String u, String n)
	{
		return null;
	}

	public FsnTwitter() throws IOException
	{
		ConfigurationBuilder cb = new ConfigurationBuilder();
		Properties props = new Properties();
		props.load(this.getClass().getClassLoader().getResourceAsStream("twitter.properties"));
		cb.setDebugEnabled(true)
		  .setOAuthConsumerKey(props.getProperty("consumer.key"))
		  .setOAuthConsumerSecret(props.getProperty("consumer.secret"))
		  .setOAuthAccessToken(props.getProperty("accesstoken"))
		  .setOAuthAccessTokenSecret(props.getProperty("accesstoken.secret"));
		twitterInstance = new TwitterFactory(cb.build()).getInstance();
	}

	public void post(String u, String at, String m)
	{
		try
		{
			if(u!=null)
				m="@"+u+" "+m;
			Status status = this.twitterInstance.updateStatus(m);
			LogManager.log("Tweeted \""+status.getText()+"\"");
		}
		catch(TwitterException e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error tweeting "+m);
		}
	}

	public void sendDirectMessage(String screenName, String m)
	{
        try {
            DirectMessage message = this.twitterInstance.sendDirectMessage(screenName, m);
            LogManager.log("Sent DirectMessage to " + message.getRecipientScreenName());
        }
        catch(TwitterException e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error sending DirectMessage to @"+screenName);
		}
	}

	public void follow(String screenName)
	{
		try {
            this.twitterInstance.createFriendship(screenName);
            LogManager.log("Following @" + screenName);
        }
		catch(TwitterException e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error following @"+screenName);
		}
	}

	public ArrayList<FsnUser> search(String user, String accessToken)
	{
		ArrayList<FsnUser> al = new ArrayList<FsnUser>();
		try
		{
			int page = 1;
			ResponseList<User> users;
			do
			{
                users = this.twitterInstance.searchUsers(user, page);
                for (User u : users)
                    al.add(new FsnUser(u.getScreenName(), u.getName(), "", "twitter"));
                page++;
            }
			while (users.size() != 0 && page < 10);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return al;
	}

	public String fetchUsername(String accessToken) {
		return null;
	}
}
