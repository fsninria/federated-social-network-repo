package org.inria.fsn.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.inria.fsn.model.db.ConnectionManager;
import org.inria.fsn.model.util.LogManager;

 public class RealtimeUpdateCallback extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
   static final long serialVersionUID = 1L;

	public RealtimeUpdateCallback() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("hub.mode").equals("subscribe") && request.getParameter("hub.verify_token").equals("FSNINRIA"))
		{
			response.getWriter().print(request.getParameter("hub.challenge"));
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message ="";
		BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
		for (String buffer;(buffer = in.readLine()) != null;message+=buffer + "\n");

		Connection conn = null;
		Statement stat = null;
		try
		{
			conn = ConnectionManager.getConnection();
			stat = conn.createStatement();
			// rs = stat.executeQuery("SELECT accesstoken FROM accesstoken WHERE username='"+keyword+"'");
		}
		catch(Exception e)
		{
			LogManager.log(e, "Error generating an event in database");
		}
	}
}