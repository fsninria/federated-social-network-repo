package org.inria.fsn.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.inria.fsn.model.interfaces.FsnNetwork;
import org.inria.fsn.model.util.FsnUtility;

 public class SendMessage extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
   static final long serialVersionUID = 1L;
	public SendMessage() {
		super();
	}   	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String toNetwork   = request.getParameter("toNetwork").toLowerCase();
		String fromNetwork = request.getParameter("fromNetwork").toLowerCase();
		String toUser      = request.getParameter("toUser").toLowerCase();
		String fromUser    = request.getParameter("fromUser").toLowerCase();
		String message     = request.getParameter("message").replace("%20", " ");
		FsnNetwork nw = FsnUtility.createNetwork(toNetwork);
		nw.sendMessage(toUser, "", fromUser, fromNetwork, message);
	}  	  	  	    
}