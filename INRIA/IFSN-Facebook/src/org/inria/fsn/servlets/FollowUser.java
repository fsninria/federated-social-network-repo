package org.inria.fsn.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.inria.fsn.model.Configuration;
import org.inria.fsn.model.FsnTwitter;
import org.inria.fsn.model.db.ConnectionManager;
import org.inria.fsn.model.util.LogManager;

 public class FollowUser extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
   static final long serialVersionUID = 1L;
	public FollowUser() {
		super();
	}   	 	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String toNetwork     = request.getParameter("toNetwork").toLowerCase();
		String fromNetwork   = request.getParameter("fromNetwork").toLowerCase();
		String userToFollow  = request.getParameter("userToFollow").toLowerCase();
		String userFollowing = request.getParameter("userFollowing").toLowerCase();
		if(toNetwork.equals("twitter"))
		{
			FsnTwitter fsnTwitter = new FsnTwitter();
			fsnTwitter.follow(userToFollow);
			fsnTwitter.post(userToFollow, "", "allow-follow "+userFollowing+":"+fromNetwork+"?YES or NO");
			Connection conn = null;
			Statement stat  = null;
			try
			{
				conn = ConnectionManager.getConnection();
				stat  = conn.createStatement();
				stat.executeUpdate("INSERT INTO accesscontrol values('"+userToFollow+"', '"+userFollowing+"', '"+toNetwork+"', '"+fromNetwork+"', '', 'pending')");
			}
			catch(Exception e)
			{
				if(Configuration.debug)
					LogManager.log(e, "Error saving pending request from user "+userFollowing+":"+fromNetwork+" to "+userToFollow+":"+toNetwork+"in the database");
			}
			finally
			{
				try {
					if(!stat.isClosed())
						stat.close();
					if(!conn.isClosed())
						conn.close();
				}
				catch(Exception e)
				{
					if(Configuration.debug)
						LogManager.log(e, "Error terminating database connection");
				}
			}
		}
	}   	  	    
}