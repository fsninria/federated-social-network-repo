package org.inria.fsn.servlets;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.inria.fsn.model.Configuration;
import org.inria.fsn.model.util.LogManager;

 public class Auth extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
   static final long serialVersionUID = 1L;

	public Auth() {
		super();
	}   	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Properties props = new Properties();
		try {
			props.load(this.getClass().getClassLoader().getResourceAsStream("facebook.properties"));
			String codeURL = props.getProperty("code_url");
			String cid = props.getProperty("client_id");
			String callbackURL = props.getProperty("callback_url");

			codeURL = codeURL.replace("{cid}", cid)
							 .replace("{authhandler}", callbackURL);
			if(!response.isCommitted())
				response.sendRedirect(codeURL);
		}
		catch(Exception e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error loading facebook app");
			if(!response.isCommitted())
				response.sendRedirect("./error.jsp?referrer=/Auth&error="+e.toString());
		}
	}  	  	  	    
}