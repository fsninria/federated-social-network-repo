package org.inria.fsn.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.inria.fsn.model.Configuration;
import org.inria.fsn.model.db.ConnectionManager;
import org.inria.fsn.model.util.FsnUtility;
import org.inria.fsn.model.util.LogManager;

 public class AuthCallbackHandler extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
   static final long serialVersionUID = 1L;

	public AuthCallbackHandler() {
		super();
	}   	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Properties props = new Properties();
		Connection conn = null;
		Statement stat  = null;
		try {
			props.load(this.getClass().getClassLoader().getResourceAsStream("facebook.properties"));
		}
		catch(Exception e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error loading facebook app");
			if(!response.isCommitted())
				response.sendRedirect("./error.jsp?referrer=/AuthCallbackHandler&error="+e.toString());
		}

		String username = null;
		String accessToken = null;
		String expiry = null;
		try {
			String accessCode = request.getParameter("code");
			String accessTokenURL = props.getProperty("access_token_url");
			String cid = props.getProperty("client_id");
			String csecret = props.getProperty("client_secret");
			String callbackURL = props.getProperty("callback_url");
			accessTokenURL = accessTokenURL.replace("{cid}", cid)
										   .replace("{csecret}", csecret)
										   .replace("{authhandler}", callbackURL)
										   .replace("{code}", accessCode);
	
			String accessURLResponse = readUrl(accessTokenURL);
			accessToken = accessURLResponse.split("&")[0].replaceFirst("access_token=", "");
			expiry      = accessURLResponse.split("&")[1].replaceFirst("expires=", "");
			username    = FsnUtility.createNetwork("facebook").fetchUsername(accessToken);
		}
		catch(Exception e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error reading the access token from facebook server");
			if(!response.isCommitted())
				response.sendRedirect("./error.jsp?referrer=/AuthCallbackHandler&error="+e.toString());
		}

		try
		{
			conn = ConnectionManager.getConnection();
			stat  = conn.createStatement();
			stat.executeUpdate("REPLACE INTO accesstoken VALUES('"+username+"', '"+accessToken+"', '"+expiry+"')");
			stat.executeUpdate("UPDATE accesstoken SET accesstoken='"+accessToken+"' WHERE username='"+username+"'");
		}
		catch(Exception e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error adding AccessToken for user "+username);
			if(!response.isCommitted())
				response.sendRedirect("./error.jsp?referrer=/AuthCallbackHandler&error="+e.toString());
		}
		finally
		{
			try {
				if(!stat.isClosed())
					stat.close();
				if(!conn.isClosed())
					conn.close();
			}
			catch(Exception e)
			{
				if(Configuration.debug)
					LogManager.log(e, "Error terminating database connection");
			}
		}
		if(!response.isCommitted())
			response.sendRedirect("./home.jsp?username="+username);
	}
	
	private String readUrl(String urlString) throws IOException {
		URL url = new URL(urlString);
		BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
		String response = "";
		String inputLine;
		while ((inputLine = in.readLine()) != null)
		response += inputLine;
		in.close();
		return response;
	}
}