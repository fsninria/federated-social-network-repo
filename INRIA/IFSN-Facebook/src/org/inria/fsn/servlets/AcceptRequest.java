package org.inria.fsn.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.inria.fsn.model.Configuration;
import org.inria.fsn.model.FsnFacebook;
import org.inria.fsn.model.db.ConnectionManager;
import org.inria.fsn.model.interfaces.FsnNetwork;
import org.inria.fsn.model.util.FsnUtility;
import org.inria.fsn.model.util.LogManager;

 public class AcceptRequest extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
   static final long serialVersionUID = 1L;

	public AcceptRequest() {
		super();
	}   	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String requestIds = request.getParameter("request_ids");
		String[] ids = requestIds.split(",");
		String query = "UPDATE accesscontrol SET status='accepted' WHERE reference='"+ids[0].trim()+"'";
		if(ids.length>1)
		{
			for(int i=1;i<ids.length;i++)
			{
				query += " OR reference='"+ids[i].trim()+"'";
			}
		}
		Connection conn = null;
		Statement stat  = null;
		try
		{
			conn = ConnectionManager.getConnection();
			stat  = conn.createStatement();
			stat.executeUpdate(query);
			FsnFacebook ff = new FsnFacebook();

			for(int i=0;i<ids.length;i++)
			{
				Statement stm = conn.createStatement();
				String searchQuery = "SELECT * FROM accesstoken WHERE username=(SELECT username FROM accesscontrol WHERE reference='"+ids[i].trim()+"')";
				ResultSet rs = stm.executeQuery(searchQuery);
				if(rs.next())
					ff.deleteNotification(rs.getString("username"), rs.getString("accesstoken"), ids[i].trim());
				stm = conn.createStatement();
				rs = stm.executeQuery("SELECT * FROM accesscontrol WHERE reference='"+ids[i].trim()+"'");
				if(rs.next())
				{
					FsnNetwork nw = FsnUtility.createNetwork(rs.getString("followernetwork"));
					nw.post(rs.getString("follower"), "", rs.getString("username")+" on "+rs.getString("usernetwork")+" accepted your request. To receive messages from your followers please follow us(@fsn_inria)");
					System.out.println("posted "+rs.getString("follower")+rs.getString("username")+" on "+rs.getString("usernetwork")+" accepted your request");
				}
				rs.close();
				stm.close();
			}
		}
		catch(Exception e)
		{
			if(Configuration.debug)
				LogManager.log(e, "Error accepting requests");
			if(!response.isCommitted())
				response.sendRedirect("./error.jsp?referrer=/AcceptRequest&error="+e.toString());
		}
		finally
		{
			try {
				if(!stat.isClosed())
					stat.close();
				if(!conn.isClosed())
					conn.close();
			}
			catch(Exception e)
			{
				if(Configuration.debug)
					LogManager.log(e, "Error terminating database connection");
			}
		}
		if(!response.isCommitted())
			response.sendRedirect("./index.jsp");
	} 	    
}