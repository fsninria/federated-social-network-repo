<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="org.inria.fsn.model.*" %>
<%@page import="org.inria.fsn.model.data.*" %>
<%@page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Federated Social Network - INRIA</title>
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
<script>
function follow(index, toNetwork, fromNetwork, userToFollow, followingUser)
{
	var xmlhttp;
	if (window.XMLHttpRequest)
	{
	  	xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    	document.getElementById(index).innerHTML="Request sent";
	    }
	}
	xmlhttp.open("GET","./FollowUser?toNetwork="+toNetwork+"&fromNetwork="+fromNetwork+"&userToFollow="+userToFollow+"&userFollowing="+followingUser,true);
	xmlhttp.send();
}
</script>
</head>
<body>
<%
String keyword   = request.getParameter("keyword");
String toNetwork = request.getParameter("toNetwork");
String followingUser = request.getParameter("followingUser");
%>
<div id="wrapper">
	<div id="header" class="container">
		<div id="logo">
			<h1><a href="./home.jsp">IFSN</a></h1>
			<p>Federated Social Network from Inria</p>
		</div>
		<div id="logo_image">
			<a href="http://www.inria.fr/en/"><img src="images/logo_INRIA_en.png"/></a>
		</div>
		<div id="menu">
			<ul>
				<li><a href="./home.jsp?username=<%=followingUser %>">Home</a></li>
			</ul>
		</div>
	</div>
	<!-- end #header -->
	<div id="page" class="container">
		<div id="content">
			<div class="post">
				<h3 class="title">Search Results for <%=keyword %></h3>
				<div style="clear: both;">&nbsp;</div>
				<div class="entry">
					<table>
						<%
						if(toNetwork.equals("twitter"))
						{
							FsnTwitter fsnTwitter = new FsnTwitter();
							ArrayList<FsnUser> al = fsnTwitter.search(keyword, "");
							for(FsnUser user : al)
							{
								out.println("<tr> <td>"+user.getFirstName()+"</td> <td>@"+user.getUsername()+"</td> <td><div id='"+user.getUsername()+"'> <button id='search-submit' type='button' onclick='follow(\""+user.getUsername()+"\", \""+toNetwork+"\", \"facebook\", \""+user.getUsername()+"\", \""+followingUser+"\")'>Follow</button> </div></td> </tr>");
							}
						}
						%>
					</table>
					<div style="clear: both;">&nbsp;</div>
					<div style="clear: both;">&nbsp;</div>
					<a href="http://facebook.com">Take me back to Facebook</a>
				</div>
			</div>
			<div style="clear: both;">&nbsp;</div>
		</div>
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->
</div>
</body>
</html>
