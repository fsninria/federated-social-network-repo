<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="org.inria.fsn.model.util.FsnUtility"%>
<%@page import="org.inria.fsn.model.data.*"%>
<%@page import="java.util.*"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Following</title>
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<%
	String username = request.getParameter("username");
	if(username==null)
	{
		response.sendRedirect("./error.jsp?referrer=home.jsp&error=Please Login With Facebook");
	}
%>
<div id="wrapper">
	<div id="header" class="container">
		<div id="logo">
			<h1><a href="./home.jsp?username=<%=username %>">IFSN</a></h1>
			<p>Federated Social Network from Inria</p>
		</div>
		<div id="logo_image">
			<a href="http://www.inria.fr/en/"><img src="images/logo_INRIA_en.png"/></a>
		</div>
		<div id="menu">
			<ul>
				<li><a href="./home.jsp?username=<%=username %>">Home</a></li>
				<li><a href="./followers.jsp?username=<%=username %>">Followers</a></li>
				<li><a href="./messages.jsp?username=<%=username %>">Messages</a></li>				
			</ul>
		</div>
	</div>
	<!-- end #header -->
	<div id="page" class="container">
		<div id="content">
			<div class="post">
				<h2 class="title">Following</h2>
				<table>
					<tr>
						<th>User Network</th>
						<th>Username</th>
						<th>Status</th>
					</tr>
					<%
						List<FsnUser> al = FsnUtility.getFollowingsOf(username, "facebook");
						for(FsnUser user : al)
						{
							out.println("<tr> <td>"+user.getNetwork()+"</td> <td>@"+user.getUsername()+"</td><td>"+user.getStatus()+"</td></tr>");
						}
					%>
				</table>
			</div>
			<div style="clear: both;">&nbsp;</div>
		</div>
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->
</div>
</body>
</html>