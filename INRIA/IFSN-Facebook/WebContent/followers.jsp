<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="org.inria.fsn.model.util.FsnUtility"%>
<%@page import="org.inria.fsn.model.data.*"%>
<%@page import="java.util.*"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Followers</title>
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
<script>
function sendmessage(index, toNetwork, fromNetwork, toUser, fromUser)
{
	var xmlhttp;
	if (window.XMLHttpRequest)
	{
	  	xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    	document.getElementById(index).value="";
	    }
	}
	var message = document.getElementById(index+"-message").value;
	document.getElementById(index+"-message").value = "";
	xmlhttp.open("GET","./SendMessage?toNetwork="+toNetwork+"&fromNetwork="+fromNetwork+"&toUser="+toUser+"&fromUser="+fromUser+"&message="+message, true);
	xmlhttp.send();
}
</script>
</head>
<body>
<%
	String username = request.getParameter("username");
	if(username==null)
	{
		response.sendRedirect("./error.jsp?referrer=home.jsp&error=Please Login With Facebook");
	}
%>
<div id="wrapper">
	<div id="header" class="container">
		<div id="logo">
			<h1><a href="./home.jsp?username=<%=username %>">IFSN</a></h1>
			<p>Federated Social Network from Inria</p>
		</div>
		<div id="logo_image">
			<a href="http://www.inria.fr/en/"><img src="images/logo_INRIA_en.png"/></a>
		</div>
		<div id="menu">
			<ul>
				<li><a href="./home.jsp?username=<%=username %>">Home</a></li>
				<li><a href="./messages.jsp?username=<%=username %>">Messages</a></li>
				<li><a href="./following.jsp?username=<%=username %>">Following</a></li>
			</ul>
		</div>
	</div>
	<!-- end #header -->
	<div id="page" class="container">
		<div id="content">
			<div class="post">
				<h2 class="title">Followers</h2>
				<table>
					<tr>
						<th>Follower Network</th>
						<th>Follower Username</th>
						<th>Send Message</th>
					</tr>
					<%
						List<FsnUser> al = FsnUtility.getFollowersOf(username, "facebook");
						for(FsnUser follower : al)
						{
							out.println("<tr> <td>"+follower.getNetwork()+"</td> <td>@"+follower.getUsername()+"</td> <td><div id='"+follower.getUsername()+"-"+follower.getNetwork()+"'> <input type='text' id='"+follower.getUsername()+"-"+follower.getNetwork()+"-message'/><button type='button' onclick='sendmessage(\""+follower.getUsername()+"-"+follower.getNetwork()+"\", \""+follower.getNetwork()+"\", \"facebook\", \""+follower.getUsername()+"\", \""+username+"\")'>Send</button> </div></td> </tr>");
						}
					%>
				</table>
			</div>
			<div style="clear: both;">&nbsp;</div>
		</div>
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->
</div>
</body>
</html>