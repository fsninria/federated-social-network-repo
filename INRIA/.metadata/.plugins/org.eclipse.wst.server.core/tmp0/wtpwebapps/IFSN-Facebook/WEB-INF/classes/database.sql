CREATE database IF NOT EXISTS IFSN;
USE IFSN;
CREATE TABLE IF NOT EXISTS AccessToken
(
	username varchar(50) NOT NULL PRIMARY KEY,
	accesstoken varchar(110) NOT NULL,
	expiry varchar(20)
);