<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Federated Social Network - INRIA</title>
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<%
	String username = request.getParameter("username");
	if(username==null)
	{
		response.sendRedirect("./error.jsp?referrer=home.jsp&error=Please Login With Facebook");
	}
%>
<div id="wrapper">
	<div id="header" class="container">
		<div id="logo">
			<h1><a href="#">IFSN</a></h1>
			<p>Federated Social Network from Inria</p>
		</div>
		<div id="logo_image">
			<a href="http://www.inria.fr/en/"><img src="images/logo_INRIA_en.png"/></a>
		</div>
		<div id="menu">
			<ul>
				<li><a href="./followers.jsp?username=<%=username %>">Followers</a></li>
				<li><a href="./messages.jsp?username=<%=username %>">Messages</a></li>
			</ul>
		</div>
	</div>
	<!-- end #header -->
	<div id="page" class="container">
		<div id="content">
			<div class="post">
				<h2 class="title"><%= username %>, Welcome to IFSN</h2>
				<div style="clear: both;">&nbsp;</div>
				<div class="entry">
					<h3>Search and follow people on other networks</h3>
					<div id="search" >
						<form method="post" action="./searchUser.jsp">
							<div>
								<p>Network :  
									<select name="toNetwork" id="search-dropdown">
										<option value="twitter">Twitter</option>
									</select>
								</p>
								<p>Username: <input type="text" id="search-text" name="keyword"/></p>
								<input type="hidden" name="followingUser" value=<%= username %>>
								<p><input type="submit" name="submit" id="search-submit" value="Search"/></p>
							</div>
						</form>
					</div>
					<h3>About IFSN</h3>
					<p>
						In the near future, the use of ICT to enrich our social interactions will grow, both in terms of size and complexity. However current online social networks (OSNs) act like data silos, storing and analyzing their users data, while locking in those very users to their servers, with non-existent support for federation; this is reminiscent of the early days of email, where one could only email those who had accounts on the same Unix machine.
					</p>
					<p>
						Our view to address the above takes inspiration from the manner in which users currently use email. While their inboxes contain an immense amount of extremely personal data, most users are happy to entrust it to corporate or personal email providers (or store and manage it individually on their personal email servers) all the while being able to communicate with users on any other email server. The notion of Federated Social Networks (FSNs) already gaining some traction envisions a similar ecosystem where users are free to choose OSN providers which will provide storage and management of their social information, while allowing customers using different OSN providers to interact socially.
					</p>
				</div>
			</div>
			<div style="clear: both;">&nbsp;</div>
		</div>
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->
</div>
</body>
</html>